![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# JAVA IDE (& SDK)

[English version](#english-version)

---
### Lernziel:
* Überblick JAVA Plattform
* Was ist eine "IDE"
* Installation einer IDE (IntelliJ)
* Integration von GitLAB in IntelliJ

--- 

## Die JAVA Plattform

![Palttform](./x_gitressourcen/Plattform.jpg)

**JDK**: JAVA Development Kit - Software Entwicklungs Kit (SDK) <br>
**JRE**: JAVA Runtime Environment - reine Ausführungsumgebung für Java <br>
**JVM**: JAVA Virtual Machine - Basis des Interpreters (java.exe) <br>
**Dev.Tools**: CL-Tools* im **bin**-Ordner der JDK:
   **javac.exe** ist der Compiler, **java.exe** ist das ausführende Programm (Interpreter) <br>
**IDE**: Integrierte Software Entwicklungsumgebung mit graph. Oberfläche

*) CL = Command Line

## Was ist eine IDE?

![Video:](../x_gitressourcen/Video.png)
[![Was ist eine IDE](https://img.youtube.com/vi/vUn5akOlFXQ/0.jpg)](https://www.youtube.com/watch?v=vUn5akOlFXQ)



## Welche IDE soll ich auswählen?

[Überblick und Empfehlung](https://www.educative.io/blog/best-java-ides-2021)

Abstimmung:

![](./x_gitressourcen/Abstimmung.png)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## JAVA SDK:

Zur Ausführung von Java brauchem wir die JDK (Java Development Kit / Aktuell Version 16) auf unserem Rechner:

[Download](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html)

Verschaffen Sie sich eine **kurzen Überblick** über die JDK auf Wikipedia:

[Überblick (Wikipedia)](https://de.wikipedia.org/wiki/Java_Development_Kit)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Installation der IDE "IntelliJ":

### IntelliJ für WIN10:

![Video:](../x_gitressourcen/Video.png)
[![IntelliJ](https://img.youtube.com/vi/EMLTOMdIz4w/0.jpg)](https://www.youtube.com/watch?v=EMLTOMdIz4w)

> **Anm**.: Vergesst nicht in der Systemeinstellung den "PATH"- und "JAVA_HOME"-Variablen den JDK-Pfad anzuhängen! Siehe im Video ab Position 5:38!

### IntelliJ für MAC:

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/8BrZ7CMrNe8/0.jpg)](https://www.youtube.com/watch?v=8BrZ7CMrNe8)


[IntelliJ Helpseite](https://www.jetbrains.com/help/idea/discover-intellij-idea.html) 

### Einführung in die Tutorials:

![Video:](../x_gitressourcen/Video.png)
[![Tutorial](https://img.youtube.com/vi/vsUx-kod2O4/0.jpg))](https://www.youtube.com/watch?v=vsUx-kod2O4)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To für APIs und Profis:

## Einbindung GitLAB/GitHUB in IntelliJ:

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/uUzRMOCBorg/0.jpg
)](https://www.youtube.com/watch?v=uUzRMOCBorg)

---

# Checkpoint
* Wozu wird eine **IDE verwendet**?
* Welches ist die **neuste Version** der Java JDK?
* Die JDK besteht aus einigen Programmen. Nenne die **zwei wichtigsten Programme**, um ein eigenes Programm ab Quellcode (Programmzeilen in Datei) laufen lassen zu können.
* Auf welchen **Plattformen** läuft JAVA?
* **Erstes Programm** in JAVA zum Laufen gebracht. (--> Tutorial: "Hello IDEA")
* Für Profis: Mein erstes JAVA Programm (Projekt) in mein Git-Repo (Server) hochgeladen.


---
---

<br><br><br>

# English version

## Learning goals:
* Overview JAVA platform
* What is an "IDE"
* Installing an IDE (IntelliJ)
* Integrating Gitlab with IntelliJ

--- 

## The JAVA platform

![Palttform](./x_gitressourcen/Plattform.jpg)

**JDK**: JAVA Development Kit  (SDK) <br>
**JRE**: JAVA Runtime Environment <br>
**JVM**: JAVA Virtual Machine - for the Interpreter (java.exe) <br>
**Dev.Tools**: CL-Tools of the **bin**-directory in JDK: **javac.exe** starts the compiler, **java.exe** runs the application within the JVM (Interpreter) <br>
**IDE**: Integrated Development Environment with graphical user interface (UI)

(CL = Command Line)

## Which IDE should I choose?

Here's an [overview and recommendation](https://www.educative.io/blog/best-java-ides-2021).

---

## JAVA SDK:

In order to run Java we need a JDK (Java Development Kit / current Version 16) on our computer:
[Download](https://www.oracle.com/java/technologies/javase-jdk16-downloads.html)

Also check out Wikipedia to get an overview of JDK:
[Overview (Wikipedia)](https://en.wikipedia.org/wiki/Java_Development_Kit)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Installing an IDE

![Video:](../x_gitressourcen/Video.png)
[![IntelliJ](https://img.youtube.com/vi/EMLTOMdIz4w/0.jpg)](https://www.youtube.com/watch?v=EMLTOMdIz4w)

> **Reminder**.: Don't forget to add the JDK-Path under  "PATH"- and "JAVA_HOME"-variables in the system settings. See video at position 5:38!

## IntelliJ on MAC:

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/8BrZ7CMrNe8/0.jpg)](https://www.youtube.com/watch?v=8BrZ7CMrNe8)


[IntelliJ help page](https://www.jetbrains.com/help/idea/discover-intellij-idea.html) 

## Intro to the Tutorials:

![Video:](../x_gitressourcen/Video.png)
[![Tutorial](https://img.youtube.com/vi/vsUx-kod2O4/0.jpg))](https://www.youtube.com/watch?v=vsUx-kod2O4)


---
![ToDo](../x_gitressourcen/ToDo.png) Do To for APIs and Pro's:

## Including GitLAB/GitHUB in IntelliJ:

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/uUzRMOCBorg/0.jpg
)](https://www.youtube.com/watch?v=uUzRMOCBorg)

---

## Checkpoint
* What do we need an IDE for?
* What is the current version of Java JDK?
* JDK consists of several programs. What are the two important programs to run your code?
* On which platform does JAVA run?
* I have completed my first java program. (--> Tutorial "Hello IDEA")
* For Pro's: I have uploaded my first java program (project) on Git.

