![TBZ Logo](../x_gitressourcen/tbz_logo.png)

[TOC]

# GIT

---
### Lernziel: (Diese Lernziele sind nicht notwendig für das Modul, werden aber für APIs und Profis empfohlen.)
* Wie funktioniert Git?
* Verwendung von Git als **Portfolio-Tool**
* Verwendng von Git als **SW-Projekt-Repo-Verwaltung**
* Installation des Desktop Tools "**GitHUB-Desktop**" 

-- 

## Was ist GIT?

GIT ist ein sogenanntes **Versionskontrollsystem** (VCS) und wurde Anfang 2005 von Linus Torvalds, dem Initiator des Linux-Kernels, entwickelt. Es erstaunt deshalb nicht, dass GIT konzeptionell ähnlich aufgebaut ist wie ein Linux-Filesystem. <br><br>
Torvalds wünschte sich ein verteiltes System, welches folgende Anforderungen erfüllt:

- Unterstützung verteilter Arbeitsabläufe (Mehrere können an einem Projekt arbeiten)
- Hohe Sicherheit gegen sowohl unbeabsichtigte als auch böswillige Verfälschung
- Effizienz (Einfach und zweckmässige Handhabung)

![Was ist Git](./x_gitressourcen/04_DVCS.jpg)

Ein Versionskontrollsystem  wie **GIT** ist grundsätzlich ein Softwaretool, mit dessen Hilfe Entwickler Quellcode kooperativ verwalten können. Die Versionskontrollsoftware verfolgt jede Änderung am Code und speichert sie in einer speziell hierfür angelegten Datenbank (im Verzeichnis **.git**). Unterläuft einem Entwickler ein Fehler, kann er zu jeder Zeit einen (oder mehrere) Schritt(e) zurück machen, seinen Code mit früheren Codeversionen abgleichen und Korrekturen implementieren.
<br>

### **VCS** vs **DVCS** ###
- **VCS** (Version Control System) <br>
Solange die Daten nur lokal mit GIT getracked werden, spricht man von **VCS**

- **DVCS** (Distributed Version Control System)<br>
Sobald die Daten zusätzlich für andere (Contributors) freigegeben werden, spricht man nicht mehr von **VCS**, sondern von  **DVCS** (**D**istributed **V**ersion **C**ontrol **S**ystem)
<br>

Hier nochmals die wichtigsten Merkmale eines **Distributed Version Control Systems**

![Was ist Git](./x_gitressourcen/01_Was-ist-GIT.jpg) 

---

## Das GIT Konzept ##



Das folgende Diagramm zeigt auf wie Git funktioniert. 

Voraussetzungen und Funktionen kurz erklärt:

* Ein Account (Education) auf einem **Git-Server** (GitHUB , GitLAB , BucketGit ) 
* Datensammlungen sind als sog. **Repositories** abgelegt
* Ein lokales Tool ermöglicht mir die Verwaltung der Repositories.
* Auf dem lokalen Rechner werden Kopien der Server-Repositories (remote-repository &#8594; lokal-repository) gespeichert. ("Fetch Origin")
* Ein Repository speichert alle Daten aller Versionen und verfügt über eine History der Versionen.
*  Eine bestimmte Version (meistens die neuste) wird als Arbeitskopie ins Verzeichnis des Rechners gelegt und ist für den Benutzer sichtbar.
*  Um bestimmte (z.B. geänderte) Dateien ins lokale Reository aufnehmen zu können, mussen die Dateien markiert werden. Dabei "landen" sie in einem Zwischenspiecher (Index oder Stage)
* Die so "gefüllte" Stage kann dann in einem Rutsch ins lokale Repo übernommen werden. Dabei werden verschiedene Bedingungen überprüft und ggf. Bestätigungen erwartet.
* Ein lokales Repo kann zur Verteilung (Team) wieder auf den Server geladen werden.

Anhand der Graphik können die dazu notwendigen GitBash Befehle eruiert werden:

  ![Konzep](./x_gitressourcen/Git Struktur.png)
  
In diesem Modul wollen wir uns nicht mit den GitBASH-Befehlen auseinandersetzen, sondern das Tool **GitHUB-Desktop** benutzen, um die einfachsten Versionierungsschritte ausführen zu können.

> (Siehe auch Screencast ab Position 6:38 weiter unten!)

### Details zu den lokalen Bereichen:

Im **Working Directory** befinden sich alle neuen und veränderten Files, die noch nicht fertig bearbeitet sind und deshalb **NICHT** ge"staged" wurden. Ein Beispiel wäre ein neues Dokument, das noch in Bearbeitung ist.

In der **Staging Area** (Index) befinden sich sämtliche Files, die verändert oder neu erstellt wurden und nun soweit fertig bearbeitet sind, um sie mit dem nächsten "Commit" in die Datenbank einzulesen.

Im **Repository** (.git-Directory) befinden sich sämtliche Daten, die bereits schon zu einem früheren Zeitpunkt "Commited" wurden und somit von GIT ge"tracked" werden.


---

# Verwendung als Portfolio Tool

Ein Git-Tool eignet sich durchaus für uns als Portfolio-Tool und kann dafür eingesetzt werden. 
Dieses m319-Repo ist ein Beispiel dafür!

Die Dateien können lokal im Arbeitsverzeichnis bearbeitet, zur Versionierung ins lokale Repo gespeichert (add / commit) und zur Verteilung im Team auf den Git-Server geladen werden.

Auch können bestehende Repos vom Git-Server zur eigenen Weiterverwendung geklont werden.

**Vorteil**: Auf dem Git-Server sind die versionierten Dateien im Browser sichtbar. Bestimmte Dateitypen sind direkt im Browser einsehbar (.md, .pdf, .txt, ...)<br>
Mit Markdown-Dateien lassen sich Inhalte einfach gestalten.

**Nachteil**: Ist eine reine Datenablage mit Schwerpunkt Software-Entwicklung

---

# Verwendung SW-Projekt Versionierung

Erstellen Sie ein Repo mit allen ihren JAVA Dateien und geben Sie diese zur Einsicht frei.

**Vorteil**: Verschiedene IDE's unterstützen direkt die Einbindung von Git-Repos.


---

# Verwendung von GitHUB-Desktop:

Profis benutzen die GitBash, um die Git-Befehle auszuführen. Als Einstieg können wir ein graphisches Tool verwenden und erste Erfahrungen mit GIT machen.

> **Anm**.: Eurem TBZ-Email wird bei GitHUB und GitLAB via Domainname erkannt und ihr könnt euch als "Studenten" registrieren.

### Folgender _Screencast_ führt euch durch die Installation und gibt eine kurze Einführung:

![Video:](../x_gitressourcen/Video.png) 14 Min
[![GitLABDesktop](./x_gitressourcen/m319_GitLAB_Desktop_View.png)](https://web.microsoftstream.com/video/9b697d68-092f-434f-9dfa-a3a11795e75e)

Erklärung zum Tool:

![](./x_gitressourcen/GitHUB_Desktop.png)


1. Angewähltes Repo
2. History Tab: Darin sind alle Veränderungen alter Verionen einsehbar
3. Changes Tab: Zeigt die aktuellen Änderungen zwischen Arbeitsverzeichnis und lokalem Repo an.
4. Geänderte Dateien können zur Stage hinzugefügt werden.
5. Rot: Diese Zeilen wurden geläscht / ersetzt
6. Grün: Diese Zeilen wurden (anstelle) hinzugefügt
7. Name des Commits (Wird in History / GitLAB-Browser angezeigt)
8. Kommentar zum Commit und Button mit Angabe in welchen Branch eingefplegt wird.
9. Button, um vom remote repo Daten zu holen (fetch) oder das lokale Repo dort abzulegen (push)
10. Wahl des aktuellen Branches (sollte "main"/"master" sein in diesem Modul)


---

# Checkpoint
* Was macht ein Versionskontrollsystem?
* Welche GIT Server kennen Sie?
* Was braucht es auf dem lokalen Rechner, um ein GIT-Repo zu unterhalten? 
* Das Konzept von Git verstehen (Screenccast ab Position 6:38)
* Zwei Möglichkeiten GIT-Kommandos auszuführen?
* Bedienung und Funktionsweise von GitHUB-Desktop


