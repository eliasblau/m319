![TBZ Logo](./x_gitressourcen/tbz_logo.png)
![m319 Picto](./x_gitressourcen/m319_picto.jpg)

[TOC]

# m319 - Applikationen entwerfen und implementieren

1.Lehrjahr: Q1/Q2 für AP, Q3/Q4 für PE

## Modulbeschreibung:

Applikationen entwerfen und implementieren

Die Lernenden kennen die Grundlagen des Programmierens, deren Herkunft, ihrer Voraussetzungen und sind in der Lage im beruflichen Umfeld Probleme zu verstehen und Lösungen dafür zu entwickeln.

In diesem Modul liegt der Fokus auf einem Kompetenzraster. Die Lernenden erbringen Kompetenznachweise durch vorgegebene Aufträge und/oder selbst-entwickelte Aufgaben oder Produkte.


[Modul_ID und Kompetenzmatrix](https://gitlab.com/modulentwicklungzh/cluster-api/m319/-/blob/master/1_Kompetenzmatrix/README.md)

---

# Themen und Unterlagen

In folgenden Ordnern befinden sich die Unterlagen zu den Themenfeldern, welche auf dem Miroboard "Learning Map m319" mit den Symbolen ![Topic](./x_gitressourcen/Topic.png) gekennzeichnet sind.

![Map](./x_gitressourcen/Learning Map.png)

## Niveau 0: "Introductions"

Hier befinden sich die einführenden Themen, die zum Setup des Moduls gehören. Folgenden Kapitel sollten durchgelesen und umgesetzt werden. 
Wenn Sie keine direkte Instruktion der Lehrpersion erhalten, lesen sie die README.md-Datei im Ordner und suchen Sie ggf. im Internet die geeignete Anleitung.


- [N0-SOL](./N0-SOL): Einführung in Selbstorganisiertes Lernen (Miroboard & Regeln)
- [N0-Portfolio](./N0-Portfolio): Wozu brauche ich ein Portfolio? Wahl eines geeigneten Portfolios!?
- [N0-Gitlab](./N0-GitLAB): GitLAB Account aufsetzen und im Browser benutzen (zur Ansicht).
Zugangslink für LP &#8594; **Team Control Center**
- [N0-JAVA-IDE](./N0-JAVA_IDE): Was ist eine IDE? <br>Varianten für Java? <br>Wahl und lokale Installation eine geeigneten IDE. <br>Evtl. Integration GitLAB (Portfolio?) für Projekte?

## Niveau 1: "Isle of Basic Skills"

Hier befinden sich die **Grundlagen** zum Programmieren. Folgende Kapitel sollten Sie **im Team** lernen und umsetzen. 


- [N1-JAVA Basics](./N1-JAVA_Basics): Einstieg in JAVA
- [N1-Variables & Constants](./N1-Variables_Constants): Wie spiechere ich Daten ab?
- [N1-UML Activity Diagram](./N1-UML_Activity_Diagram): Wie zeichne ich ein Programmfluss Diagramm?
- [N1-Flow Control](./N1-Flow_Control): Wie steuere ich den Ablauf in einem Programm?
- [N1-Debugger](./N1-Debugger): Wie teste ich mein Programm?

## Niveau 2: "Mainland of Advanced Skills"

Hier befinden sich die **zentralen Themen** des Moduls. Folgende Kapitel sollten **im Team** umgesetzt werden. 

- [N2-Task Description](./N2-Task_Description): Wie formuliere einen Auftrag?
- [N2-Code Comments](./N2-Code_Comments): Wie kann ich meinen Code wartbarer machen?
- [N2-Logicals](./N2-Logicals): Wie erstelle ich logische Bedingungen?
- [N2-Classes, Objects & Methods](./N2-Classes_Objects_Methods): Von der Klasse zum Objekt und dessen Methoden.
- [N2-Type_Conversions](./N2-Type_Conversions): Wie werden Daten eines Typs in einen anderen Typ umgewandelt?
- [N2-Testing](./N2-Testing): Wie kann ich ein Programm methodisch testen?

## Niveau 3: "Mainland of Advanced Skills"
Hier befinden sich die **zentralen Themen** des Moduls. Folgende Kapitel sollten im **Team oder einzeln** umgesetzt werden. 

- [N3-EVA Principle](./N3-EVA_Principle): Wie strukturiere ich ein einfaches Programm?
- [N3-Composite Datatypes](./N3-Composite_Datatypes): Strings und Arrays
- [N3-Code Formatting](./N3-Code_Formatting): Wie kann ich meinen Code lesbarer machen?
- [N3-More AD](./N3-More_AD): Wie lassen sich komplexe Abläufe visuell beschreiben?
 
## Niveau 4: "Promised Land of Expert Skills"

In diesem Neiveau setzen Sie das Gelernte an eigenen Produkten um und **vertiefen Ihr Können** dabei. Folgenden Kapitel sollten **einzeln** umgesetzt werden. 

- [N4-Call by](./N4-Call_by): Wie wird der Paramter-Inhalt bei beim Aufruf einer Funktion/Methode übergeben?
- [N4-Programming_Hints](./N4-Programming_Hints): Tipps und Tricks


## Knowledge Castle

[Knowledge_Library](./Knowledge_Library): Die **wichtigsten Ressourcen** sind in der Bibliothek des Schlosses zusammengestellt. Referenzen dazu finden Sie in den vorhergehenden Kapiteln. 

Auch zusätzliches Material liegt hier bereit... Bedienen Sie sich!


