![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

[TOC]

# Kompetenznachweis D3


# Auftrag: (Gruppenarbeit)

Methoden der zusammengesetzten Datentypen sind bekannt und können verwendet werden (z.B. toString). Kann zusammengesetzte Datentypen initialisieren und Zuweisungen vornehmen. (z.B. Array, String).

## Anforderungen

* Wie werden komplexe Datentypen in Objekte abgebildet? (z.B. Array, String)
* Korrekte Deklarierung, Initialisierung und Anwendung von zusammengesetzten Datentypen (Array, String, etc.).
* Wie werden Methoden von Objekten aufgerufen und wozu dienen sie? (API Dokumentation lesen)
* Wie kann ich eigene zusammengesetzte Datentypen und geeignete Methoden erstellen? (eigene Klasse)
* Wie kann der Gültigkeitsbereich von Variablen in Objekten bestimmt werden?
* Wie können Daten in verschiedene Datentypen konvertiert wurden (Runden, Trimmen, Fehler)